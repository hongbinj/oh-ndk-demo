#pragma once

#ifndef LOCAL_API
#if (defined __GNUC__) && (__GNUC__ >= 4)
#define LOCAL_API __attribute__((visibility("hidden")))
#else
#define LOCAL_API
#endif // __GNUC__
#endif // LOCAL_API

#ifndef PUBLIC_API
#if (defined __GNUC__) && (__GNUC__ >= 4)
#define PUBLIC_API __attribute__((visibility("default")))
#else
#define PUBLIC_API
#endif // __GNUC__
#endif // PUBLIC_API
