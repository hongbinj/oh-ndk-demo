#include "math.h"
#include "math_internal.h"

int add_int(int a, int b) {
    return a + b;
}

int sub_int(int a, int b) {
    return (int) dummy_sub_long(a, b);
}

int add_int2(int a, int b) {
    return a + b;
}
