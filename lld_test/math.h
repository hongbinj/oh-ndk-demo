#pragma once

#include "api.h"

PUBLIC_API int add_int(int a, int b);

PUBLIC_API int sub_int(int a, int b);

PUBLIC_API int add_int2(int a, int b);
