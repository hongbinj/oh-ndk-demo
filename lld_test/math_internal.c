#include "math_internal.h"

extern long dummy_mul_long(long a, long b);

long dummy_add_long(long a, long b) {
    return dummy_mul_long(a, 1L) + dummy_mul_long(b, 1L);
}

long dummy_sub_long(long a, long b) {
    return a - b;
}
