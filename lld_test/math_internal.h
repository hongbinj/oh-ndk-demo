#pragma once

#include "api.h"

LOCAL_API long dummy_add_long(long a, long b);
LOCAL_API long dummy_sub_long(long a, long b);
