# Using OpenHarmony NDK

## 1. Building

### 1.1 Pre-requisites

1. Install `cmake` and `ninja` on host (optional)

Recommand using the package manager of the operating system you are using, e.g.
`apt` on Ubuntu

```
apt install cmake ninja-build
```

2. Install OpenHarmony NDK

Download the NDK from [OpenHarmony CI](http://ci.openharmony.cn), extract it and
set the environment variable `OH_NDK_HOME` set to the directory where NDK is
located.

Assuming the OH SDK is in `$HOME/Downloads/ohos-sdk/current/ohos-sdk` and OS you
are using is Ubuntu

```
echo 'export OH_NDK_HOME=$HOME/Downloads/ohos-sdk/current/ohos-sdk/linux/native' >> ~/.bashrc
source ~/.bashrc
```

3. Install Android NDK (optional)

Select the NDK from [Android NDK Downloads](https://developer.android.com/ndk/downloads).
Environment variable `ANDROID_NDK_HOME` is set for convenience with the same
way.

```
echo 'export OH_NDK_HOME=$HOME/Downloads/ohos-sdk/current/ohos-sdk/linux/native' >> ~/.bashrc
source ~/.bashrc
```

### 1.2 Building for multiple platforms

| Platform                | Script                   |
| ----------------------- | ------------------------ |
| Host                    | `build_host.sh`          |
| OpenHarmony armeabi-v7a | `build_oh_armv7a.sh`     |
| OpenHarmony arm64-v8a   | `build_oh_arm64v8a.sh`   |
| Android armeabi-v7a     | `build_aosp_armv7a.sh`   |
| Android arm64-v8a       | `build_aosp_arm64v8a.sh` |

These scripts accept additional cmake options, e.g.

```
./build_oh_arm64v8a.sh -DCMAKE_BUILD_TYPE=Debug
```
