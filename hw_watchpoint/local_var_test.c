#include <stdlib.h>

int dummy_add(int a, int b) {
    volatile int sum = a + b;
    return sum;
}

int dummy_sub(int a, int b) {
    volatile int diff = a - b;
    return diff;
}

int main(void) {
    dummy_add(1, 2);
    dummy_sub(2, 1);

    return EXIT_SUCCESS;
}
