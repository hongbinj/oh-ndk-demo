#!/usr/bin/env sh

if [ -z $OH_NDK_HOME ]; then
    echo "Error: cannot find OpenHarmony NDK, environment variable OH_NDK_HOME is not set or empty."
    echo "The value of OH_NDK_HOME should be the directory of NDK. Make sure you have the NDK and set OH_NDK_HOME properly."
    echo "For example, OH_NDK_HOME can be set to '\$HOME/Downloads/ohos-sdk/current/ohos-sdk/linux/native'."
    exit 1
fi

curr_dir=$(cd $(dirname $0) && pwd)

oh_ndk_cmake=$OH_NDK_HOME/build-tools/cmake/bin/cmake
oh_ndk_ninja=$OH_NDK_HOME/build-tools/cmake/bin/ninja
oh_ndk_cmake_toolchain_file=$OH_NDK_HOME/build/cmake/ohos.toolchain.cmake
outdir=$curr_dir/out/oh-arm
srcdir=$curr_dir

$oh_ndk_cmake -B $outdir -S $srcdir \
    -G Ninja -DCMAKE_MAKE_PROGRAM=$oh_ndk_ninja \
    -DCMAKE_TOOLCHAIN_FILE=$oh_ndk_cmake_toolchain_file \
    -DOHOS_PLATFORM=ohos \
    -DOHOS_ARCH=armeabi-v7a \
    -DOHOS_STL=c++_static \
    $@

$oh_ndk_ninja -C $outdir
