## Interworking ARM and Thumb

### 1. What does interworking mean?

[Interworking ARM and Thumb][1] means, for a shared object or an executable targeted 32-bit ARM
processors, especially those implementing `ARMv7-A`, it contains both instruction sets.

One of the main purposes of interworking is to increse code density and therefore reduce memory
footprint at runtime. For example, function `mul_int` at [thumb\_lib.c](thumb_lib.c) can be compiled with Thumb instruction sets into:

```
00007398 <mul_int>:
    7398: 70 b5        	push	{r4, r5, r6, lr}
    739a: 00 29        	cmp	r1, #0
    739c: 13 d0        	beq	0x73c6 <mul_int+0x2e>   @ imm = #38
    739e: 05 46        	mov	r5, r0
    73a0: 08 46        	mov	r0, r1
    73a2: 0c 46        	mov	r4, r1
    73a4: 48 bf        	it	mi
    73a6: 60 42        	rsbmi	r0, r4, #0
    73a8: 46 1c        	adds	r6, r0, #1
    73aa: 00 20        	movs	r0, #0
    73ac: 04 e0        	b	0x73b8 <mul_int+0x20>   @ imm = #8
    73ae: 00 f0 12 e8  	blx	0x73d4 <sub_int>        @ imm = #36
    73b2: 01 3e        	subs	r6, #1
    73b4: 01 2e        	cmp	r6, #1
    73b6: 05 d9        	bls	0x73c4 <mul_int+0x2c>   @ imm = #10
    73b8: 29 46        	mov	r1, r5
    73ba: 00 2c        	cmp	r4, #0
    73bc: f7 d4        	bmi	0x73ae <mul_int+0x16>   @ imm = #-18
    73be: 00 f0 0e e8  	blx	0x73dc <add_int>        @ imm = #28
    73c2: f6 e7        	b	0x73b2 <mul_int+0x1a>   @ imm = #-20
    73c4: 70 bd        	pop	{r4, r5, r6, pc}
    73c6: 00 20        	movs	r0, #0
    73c8: 70 bd        	pop	{r4, r5, r6, pc}
```

With ARM instruction sets, the function may be compiled into:

```
00007398 <mul_int>:
    7398: 70 40 2d e9  	push	{r4, r5, r6, lr}
    739c: 00 00 51 e3  	cmp	r1, #0
    73a0: 10 00 00 0a  	beq	0x73e8 <mul_int+0x50>   @ imm = #64
    73a4: 00 50 a0 e1  	mov	r5, r0
    73a8: 01 00 a0 e1  	mov	r0, r1
    73ac: 01 40 a0 e1  	mov	r4, r1
    73b0: 00 00 64 42  	rsbmi	r0, r4, #0
    73b4: 01 60 80 e2  	add	r6, r0, #1
    73b8: 00 00 a0 e3  	mov	r0, #0
    73bc: 03 00 00 ea  	b	0x73d0 <mul_int+0x38>   @ imm = #12
    73c0: 0d 00 00 eb  	bl	0x73fc <sub_int>        @ imm = #52
    73c4: 01 60 46 e2  	sub	r6, r6, #1
    73c8: 01 00 56 e3  	cmp	r6, #1
    73cc: 04 00 00 9a  	bls	0x73e4 <mul_int+0x4c>   @ imm = #16
    73d0: 05 10 a0 e1  	mov	r1, r5
    73d4: 00 00 54 e3  	cmp	r4, #0
    73d8: f8 ff ff 4a  	bmi	0x73c0 <mul_int+0x28>   @ imm = #-32
    73dc: 08 00 00 eb  	bl	0x7404 <add_int>        @ imm = #32
    73e0: f7 ff ff ea  	b	0x73c4 <mul_int+0x2c>   @ imm = #-36
    73e4: 70 80 bd e8  	pop	{r4, r5, r6, pc}
    73e8: 00 00 a0 e3  	mov	r0, #0
    73ec: 70 80 bd e8  	pop	{r4, r5, r6, pc}
```

50 bytes v.s. 88 bytes.

[1]: https://developer.arm.com/documentation/dui0471/m/interworking-arm-and-thumb
