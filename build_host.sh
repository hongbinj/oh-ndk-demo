#!/usr/bin/env sh

curr_dir=$(cd $(dirname $0) && pwd)

build_dir=$curr_dir/out/host
src_dir=$curr_dir

cmake -B $build_dir -S $src_dir -G Ninja \
    $@

ninja -C $build_dir
