#add_executable(getpass_test getpass_test.c)
add_library(dump_auxv STATIC dump_auxv.c)

add_executable(printf_test printf_test.c)
target_link_libraries(printf_test PRIVATE dump_auxv)

add_executable(execv_test execv_test.c)
target_link_libraries(execv_test PRIVATE dump_auxv)

add_executable(memcpy_test memcpy_test.c)
