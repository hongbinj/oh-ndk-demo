#include <assert.h>
#include <string.h>

int main(void) {
  char src[1000] = {};
  char dest[1000];
  const size_t num_copy_max = 64;
  for (size_t num_copy = 0; num_copy < num_copy_max; num_copy++) {
    assert(num_copy <= sizeof(src) && num_copy <= sizeof(dest) &&
      "number of bytes to copy exceeds the size of src or dest buffer");
    memcpy(dest, src, num_copy);
  }
  return 0;
}
