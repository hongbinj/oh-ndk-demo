#include "dump_auxv.h"

#include <stdio.h>
#include <stdlib.h>

int main(void) {
    dump_auxv();
    printf("Hello world!\n");
    return EXIT_SUCCESS;
}
