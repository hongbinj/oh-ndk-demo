#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// extern char *getpass(const char *prompt);

int main(void) {
    const char *pass = getpass("Enter your password: ");

    if (pass == NULL) {
        perror("failed to get password from terminal");
        return EXIT_FAILURE;
    }

    printf("Your password is %s\n", pass);

    return EXIT_SUCCESS;
}
