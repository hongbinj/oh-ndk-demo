#include "dump_auxv.h"

#include <stdio.h>
#include <sys/auxv.h>
#include <sys/types.h>
#include <unistd.h>

void dump_auxv(void) {
    printf("Base address of dynamic loader for process %ld: 0x%lX\n",
        (long) getpid(), getauxval(AT_BASE));
}
